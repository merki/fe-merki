module.exports = {
  rootDir: '../../',
  roots: ['src/client'],
  preset: 'jest-preset-angular',
  setupTestFrameworkScriptFile: './src/client/setup-jest.ts',
  globals: {
    'ts-jest': {
      tsConfig: 'src/client/tsconfig.spec.json',
      tringifyContentPathRegex: true,
    },
  },
  coverageDirectory: './coverage/client',
  collectCoverageFrom: ['src/client/**/*.{ts,js,jsx}'],
};
